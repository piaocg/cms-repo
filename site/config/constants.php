<?php

return [
   'taxonomy' => [
      'post_category' => 'post-category',
      'post_tag' => 'post-tag',
   ]
];