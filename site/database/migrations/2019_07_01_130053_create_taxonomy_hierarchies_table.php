<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyHierarchiesTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('taxonomy_hierarchies', function (Blueprint $table) {
         $table->bigInteger('taxonomy_id');
         $table->bigInteger('parent_id');
         $table->integer('order_no');
         $table->timestamps();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::dropIfExists('taxonomy_hierarchies');
   }
}
