<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('posts', function (Blueprint $table) {
         $table->bigIncrements('id');
         $table->string('uid')->index();
         $table->bigInteger('user_id')->index();
         $table->string('title')->index();
         $table->text('excerpt')->nullable();;
         $table->text('content')->nullable();
         $table->string('status'); // draft, publish
         $table->string('content_format', 10)->default('md');
         $table->bigInteger('parent_id')->nullable();
         $table->integer('comment_count');
         $table->timestamps();
         $table->softDeletes();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::dropIfExists('posts');
   }
}
