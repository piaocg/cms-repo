<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyPostsTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('taxonomy_posts', function (Blueprint $table) {
         $table->bigInteger('taxonomy_id');
         $table->bigInteger('post_id');
         $table->integer('order_no');
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::dropIfExists('taxonomy_posts');
   }
}
