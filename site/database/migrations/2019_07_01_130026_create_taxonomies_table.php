<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomiesTable extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('taxonomies', function (Blueprint $table) {
         $table->bigIncrements('id');
         $table->string('uid')->index();
         $table->bigInteger('user_id');
         $table->string('term_name')->index();
         $table->string('name')->index();
         $table->text('description');
         $table->integer('post_count');
         $table->timestamps();
         $table->softDeletes();
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::dropIfExists('taxonomies');
   }
}
